#
#	   Copyright 2016 Jason S. Harris
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#
#
#
#   This script simulates one hour of satellites orbiting the Earth.
#
#	Usage: propagator.py [year] [month] [day]
#	This file is designed to run on an HPC cluster with Sun Grid Engine
#   The hour is passed via an environmental variable called SGE_TASK_ID
#   This is set by the qsub command when the job is scheduled. 
#


from functools import partial
import math
from multiprocessing import Pool
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv
import sys
import time

# 3D distance formula 
def distance(s1, s2):

	x1 = s1[3]
	y1 = s1[4]
	z1 = s1[5]
	
	x2 = s2[3]
	y2 = s2[4]
	z2 = s2[5]
	
	return math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1))

# format time for logging
def format_time(y, mon, d, h, min, s):
	return "%02i/%02i/%s %02i:%02i:%02i" % (mon,d,y, h, min, s)

# simulate the orbit at a specific time and get the coordinates
def propagate(TLEs, year, mon, day, hour, min, sec):
	
	satellite = TLEs
	
	line1 = satellite[1]
	line2 = satellite[2]
	
	#return satellite[0]
	
	s = twoline2rv(line1, line2, wgs72)
	
	position, velocity = s.propagate(year, mon, day, hour, min, sec)
			
	return satellite+list(position)

# get the satellite ID from a TLE list
def sat_id(s):
	
		line2 = s[2]
		
		parts = line2.split(' ')
		
		return parts[1]	

def detect_collision(TLEs, year, month, day, hour, tv):
	
	minute = math.floor((tv/60))
	second = tv % 60
			
	# the complexity of this is (N^2 - N)/2
	# where N is the number of satellites

	results = []
	collisions = []

	for sat in TLEs:
		results.append(propagate(sat, year, month, day, hour, minute, second))
		
	for y in range(0, len(results)):
			
		for z in range(y+1, len(results)):
			
			dist = distance(results[y], results[z])
		
			# if the distance between the two satellites is
			# less than 100 km, log it
				
			if dist < 100:
				
				s1 = sat_id(results[y])
				s2 = sat_id(results[z])
				
				if s1 != s2:
					tf = format_time(year, month, day, hour, minute, second)
					
					collisions.append("%s\t%s\t%s\t%s\t%s\t%s" % (tf, results[y][0].strip(), s1, results[z][0].strip(), s2, dist))
	return collisions


if __name__ == "__main__":

	year = int(sys.argv[1])
	month = int(sys.argv[2])
	day = int(sys.argv[3])
		
	#hour = int(sys.argv[4])-1	
	
	# load TLEs from file
	
	TLEs = []

	f = open("TLEs.txt", "r")

	lines = f.read().split('\n')

	f.close()

	lines = lines[:len(lines)-1]

	for x in range(0, int(len(lines)/3)):
		
		TLE = [lines[x*3], lines[x*3+1], lines[x*3+2]]
		
		TLEs.append(TLE)
		
	# start the thread pool
	
	p = Pool()
	
	# simulate an hour
	
	tv = range(0, 3600)

	for hour in range(0, 24):

		results = p.map(partial(detect_collision, TLEs, year, month, day, hour), tv)

		for result in results:

			for collision in result:
	
				print(collision)



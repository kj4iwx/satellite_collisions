#
#          Copyright 2016 Jason S. Harris
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#
#
#
#	This program retrieves a list of files from Celestrak and stores them in a
#   single text file. The resulting file is loaded by propagate.py in order to
#   run the simulation.
#

import http.client

file_list = ['amateur.txt', 'noaa.txt', 'weather.txt', 'cubesat.txt', 'cosmos-2251-debris.txt', 'iridium-33-debris.txt', 'goes.txt', 'resource.txt', 'sarsat.txt', 'tdrss.txt', 'geo.txt', 'intelsat.txt', 'gorizont.txt', 'raduga.txt', 'molniya.txt', 'iridium.txt', 'orbcomm.txt', 'globalstar.txt', 'other-comm.txt', 'gps-ops.txt', 'glo-ops.txt', 'science.txt', 'engineering.txt'];

TLEs = []

for l in file_list:

	url = "/NORAD/elements/%s" % (l)

	h = http.client.HTTPConnection("celestrak.com")
	
	h.request("GET", url)

	response = h.getresponse()
	
	data = response.read()
	
	h.close()
	
	data = data.decode('utf-8')
	
	lines = data.split('\r\n')
	
	TLEs = TLEs + lines[:len(lines)-1]


f = open("TLEs.txt", "w")

for TLE in TLEs:

	f.write(TLE)
	f.write("\n")

f.close()

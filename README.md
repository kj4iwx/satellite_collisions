# README #

** This program needs the SGP4 Library From Brandon Rhodes at https://github.com/brandon-rhodes/python-sgp4 **

This program simulates the orbits of satellites and attempts to determine collisions between them. It is designed to run on an HPC cluster
with Sun Grid Engine.

In order to run the code, first you must execute fetch_TLEs.py which downloads
the elements from Celestrak.

Next, you can run propagate3.py with 3 argumental variables. The usage is shown below

propagate3.py [year] [month] [day]

By default, any satellites passing within 100km of each other are logged. The output is printed to stdout in the format shown below where the fields are seperated by tabs. Each entry ends with a newline.


[date] [name of satellite 1] [satellite id 1] [name of satellite 2] [satellite id 2] [distance]


propagate_job.sh is a batch job which can be run under the sun grid engine and uses task arrays for the days of the month. It can be submitted as follows in order to simulate 30 days of a month.

qsub -cwd -t 1-30 ./propagate_job.sh

Results from this program were published in my Masters Thesis "Analysis and Implementation of Communications Systems for Small Satellite Missions" which you can download and read at https://digitalcommons.odu.edu/ece_etds/8/
